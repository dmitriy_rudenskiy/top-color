<?php
namespace TopColors\Util;

class Color
{
    /**
     * Порядковый номер
     *
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $index;

    public function __construct($position, $index)
    {
        $this->position = $position;
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }
}