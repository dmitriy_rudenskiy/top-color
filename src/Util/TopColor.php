<?php
namespace TopColors\Util;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use InvalidArgumentException;
use KMeans\Cluster;

class TopColor
{
    const MAX_SIZE = 100;

    /**
     * @var \Imagine\Gd\Image|\Imagine\Image\ImageInterface
     */
    private $image;

    /**
     * @var array
     */
    private $colors;

    public function __construct($filename)
    {
        $filename = realpath($filename);

        if (empty($filename)) {
            throw new InvalidArgumentException();
        }

        // открываем файл
        $imagine = new Imagine();
        $this->image = $imagine->open($filename);
        $this->image->resize(new Box(self::MAX_SIZE, self::MAX_SIZE));
    }

    public function get()
    {
        $this->getCountColor();

        return $this->clusterColor();
    }

    protected function getCountColor()
    {
        $size   = $this->image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        $this->colors = [];

        for ($x = 0;  $x < $width; $x++) {
            for ($y = 0;  $y < $height; $y++) {
                $index = imagecolorat($this->image->getGdResource(), $x, $y);

                if (!isset($this->colors[$index])) {
                    $this->colors[$index] = 0;
                }

                $this->colors[$index]++;
            }
        }
    }

    /**
     *
     */
    protected function clusterColor()
    {
        $array = [];

        $space = new \KMeans\Space(3);

        foreach ($this->colors as $key => $value) {
            $color = new \Color($key);
            $array[] = array_values($color->toXyz());
            $space->addPoint(array_values($color->toXyz()), (object)['count' => $value, 'color' => $key]);
        }

        $clusters = $space->solve(12);

        $result = [];

        foreach ($clusters as $cluster) {
            if (count($cluster) > 0) {
                $data = $this->findInCluster($cluster);
                $color = new \Color($data->color);
                $result[$color->toHex()] = $data->count;
            }
        }

        arsort($result);

        return $result;
    }

    /**
     * После разбиения по кластерам сортируем
     * по весу каждого кластера
     * и берём самы проявленный цвет в кластире
     *
     * @param \KMeans\Cluster $cluster
     * @return object
     */
    protected function findInCluster(Cluster $cluster)
    {
        $color = null;
        $count = 0;
        $max = 0;

        foreach ($cluster as $value) {
            $data = $value->toArray()['data'];

            if ($data->count > $max) {
                $max = $data->count;
                $color = $data->color;
            }

            $count += $data->count;
        }

        return (object)['count' => $count, 'color' => $color];
    }
}