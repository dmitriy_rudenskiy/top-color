<?php
namespace TopColors\Util;

interface Colors
{
    /**
     * Черный
     */
    const COLOR_BLACK = 0;

    /**
     * Темно-синий
     */
    const COLOR_NAVY = 128;

    /**
     * Голубой
     */
    const COLOR_BLUE = 255;

    /**
     * Зеленый
     */
    const COLOR_GREEN = 32768;

    /**
     * Серо-зеленый
     */
    const COLOR_TEAL = 32896;

    /**
     * Ярко-зеленый
     */
    const COLOR_LIME = 65280;

    /**
     * Морская волна
     */
    const COLOR_AQUA = 65535;

    /**
     * Темно-бордовый
     */
    const COLOR_MAROON = 8388608;

    /**
     * Фиолетовый
     */
    const COLOR_PURPLE = 8388736;

    /**
     * Оливковый
     */
    const COLOR_OLIVE = 8421376;

    /**
     * Серый
     */
    const COLOR_GRAY = 8421504;

    /**
     * Серебряный
     */
    const COLOR_SILVER = 12632256;

    /**
     * Красный
     */
    const COLOR_RED = 16711680;

    /**
     * Фуксин
     */
    const COLOR_FUCHSIA = 16711935;

    /**
     * Желтый
     */
    const COLOR_YELLOW = 16776960;

    /**
     * Белый
     */
    const COLOR_WHITE = 16777215;
}