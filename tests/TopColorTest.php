<?php
namespace Test;

use TopColors\Util\TopColor;

class TopColorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provider
     */
    public function testCountColor($filename)
    {
        $filename = __DIR__ . $filename;

        $service = new TopColor($filename);
        $list = $service->get();

        $this->assertNotEmpty(sizeof($list));
    }

    public function provider()
    {
        return [
            ['/img/pantone_001.jpg', 100, 100],
            ['/img/pantone_002.png', 100, 100]
        ];
    }
}