<?php
namespace Test;

use KMeans\Space;


class KmeansTest extends \PHPUnit_Framework_TestCase
{
    public function testClusteredData()
    {
        $points = [
            [1, 1, 3],
            [3, 5, 6],
            [5, 4, 3],
            [1, 2, 1],
            [9, 10, 8],
            [4, 4, 4]
        ];

        // Три координаты (x, y, z)
        $space = new Space(3);

        foreach ($points as $key => $value) {
            $space->addPoint($value, ['position' => $key]);
        }

        // на какое количество кластеров нужно разбить
        $clusters = $space->solve(3);

        $sum = 0;

        foreach ($clusters as $value) {
            $sum += count($value);
        }

        $this->assertEquals(sizeof($points), $sum);
    }
}