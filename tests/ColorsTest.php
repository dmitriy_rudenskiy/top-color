<?php
namespace Test;

use TopColors\Util\Colors;
use TopColors\Util\Color;

class ColorsTest extends \PHPUnit_Framework_TestCase
{
    public function testCountColor()
    {
        $inputColors = [
            Colors::COLOR_BLACK => new Color(0, 0),
            Colors::COLOR_NAVY => new Color(1, 128),
            Colors::COLOR_BLUE => new Color(2, 255),
            Colors::COLOR_GREEN => new Color(3, 32768),
            Colors::COLOR_TEAL => new Color(4, 32896),
            Colors::COLOR_LIME => new Color(5, 65280),
            Colors::COLOR_AQUA => new Color(6, 65535),
            Colors::COLOR_MAROON => new Color(7, 8388608),
            Colors::COLOR_PURPLE => new Color(8, 8388736),
            Colors::COLOR_OLIVE=> new Color(9, 8421376),
            Colors::COLOR_GRAY => new Color(10, 8421504),
            Colors::COLOR_SILVER => new Color(11, 12632256),
            Colors::COLOR_RED => new Color(12, 16711680),
            Colors::COLOR_FUCHSIA=> new Color(13, 16711935),
            Colors::COLOR_YELLOW => new Color(14, 16776960),
            Colors::COLOR_WHITE=> new Color(15, 16777215)
        ];

        foreach ($inputColors as $key => $value) {
            $this->assertEquals($key, $value->getPosition());
        }
    }
}


