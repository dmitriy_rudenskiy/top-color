<?php
namespace Test;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

class ImagineTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provider
     */
    public function testResizeImage($filename, $width, $height)
    {
        $filename = __DIR__ . $filename;

        $imagine = new Imagine();
        $image = $imagine->open($filename);
        $image->resize(new Box($width, $height));
        $size = $image->getSize();


        $this->assertEquals($width, $size->getWidth());
        $this->assertEquals($height, $size->getHeight());
    }

    public function provider()
    {
        return [
            ['/img/pantone_001.jpg', 100, 100],
            ['/img/pantone_002.png', 100, 100]
        ];
    }
}